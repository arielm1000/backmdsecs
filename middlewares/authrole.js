const Usuario = require('../models/Usuario');
const Role = require('../models/Role');

exports.authAdmin = async (req, res, next) => {
try { 
    //const use = await Usuario.findById(req.usuario._id)
    console.log('authAdmin')
    //console.log(Object.values(req.usuario));
    //console.log(Object.keys(req.usuario));
    //ya se por auth q es un usuario
    const user = await Usuario.findById(req.usuario.id);
    //console.log(user);
    //busco los roles de este usuario
    const todos = await Role.find();
    console.log(todos);
    const roles = await Role.find({ _id: { $in: user.roles} });
    //console.log(roles);
    for(let i = 0; i < roles.length; i++) {
        if (roles[i].nombre === 'admin' ) {
            next();
            return;
        }
    }
    return res.status(403).json({ message: 'Require Autorizacion'})
    } catch  (error) { 
        console.error(error);
    }
};
exports.authUser = async (req, res, next) => {
    try { 
        console.log('authUser')
        const user = await Usuario.findById(req.usuario.id);
        const roles = await Role.find({ _id: { $in: user.roles} });

        for(let i = 0; i < roles.length; i++) {
            if (roles[i].nombre === 'admin' || roles[i].nombre === 'user' || roles[i].nombre === 'moderator' ) {
                next();
                return;
            }
        }
        return res.status(403).json({ message: 'Require Autorizacion'})
        } catch  (error) { 
            console.error(error);
        }
};
exports.authModerator = async (req, res, next) => {
    try { 
        console.log('autModerator')
        const user = await Usuario.findById(req.usuario.id);
        const roles = await Role.find({ _id: { $in: user.roles} });

        for(let i = 0; i < roles.length; i++) {
            if (roles[i].nombre === 'admin' || roles[i].nombre === 'moderator' ) {
                next();
                return;
            }
        }
        return res.status(403).json({ message: 'Require Autorizacion'})
        } catch  (error) { 
            console.error(error);
        }
};