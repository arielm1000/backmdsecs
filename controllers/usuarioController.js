const Usuario = require('../models/Usuario');
const Role = require('../models/Role');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
require('dotenv').config();
 
exports.crearUsuario = async (req, res)  => {
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() });
    }

    // Extraer nombre, email y password
    const { nombre, email, password, roles } = req.body;
    try {
        let usuario = await Usuario.findOne({ email });

        if(usuario){
            return res.status(400).json({ msg: 'El email ingresado ya existe.'});
        }        
        // Creamos el usuario
        //usuario = new Usuario(req.body);
        usuario = new Usuario({nombre, email, password});
        
        // Hashear password
        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash(password, salt);

        //Busco los id roles
        if (roles) {
            const foundRoles = await Role.find({nombre: {$in: roles}});
            usuario.roles = foundRoles.map(role => role._id);
        } else {
            const role = await Role.findOne({nombre: 'user'});
            console.log('role '+role)
            //usuario.roles = role.map(role => role._id);
            usuario.roles = [role._id];
        }

        // Guardamos el usuario en la BD
        await usuario.save();
        
        // Crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };
        
        // Firmar token
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: 3600 // 1 hora
        }, (error, token) => {
            if(error) throw error;
        
            // Mensaje de confirmación
            res.json({ msg: 'Usuario creado correctamente', token });
        });

    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
