const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
 
exports.login = async (req, res)  => {
    // Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({ errors: errores.array() })
    }
    
    // Extraer email y password
    const { email, password } = req.body;
    let prueba = 0;

    try {
        // Revisar que sea un usuario registrado
        let usuario = await Usuario.findOne({ email }).populate('roles');
        
        if(!usuario){
            console.log('El usuario no existe');
            return res.status(400)
                .json({ msg: 'El usuario o contraseña son incorrectos.' });
        }

        // Revisar el password
        const passCorrecto = await bcryptjs.compare(password,usuario.password);
        if(!passCorrecto){
            console.log('El password es incorrecto');
            return res.status(400)
                .json({ msg: 'El usuario o contraseña son incorrectos.' });
        } else {console.log('El password es Correcto');}

        // Todo correcto
        // Crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };
        
        //solo envio datos del user
        usuario.password ="";
        // Firmar token
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: '5h'
        }, (error, token) => {
            if(error) throw error;
        
            // Mensaje de confirmación
            //res.json({ token });
            res.json({ token, usuario });
        });
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};