const Beneficiario = require("../models/Beneficiario");
const { validationResult } = require("express-validator");
const mongoose = require('mongoose');
const {DBFFile} = require('dbffile');

//actualizar actbeneficiarios
exports.actbeneficiarios = async (req,res) =>{
    try {
      console.log("ingresa a actbeneficiarios");
       //leer la tabla de donde voy a actualizar C:\Ariel\BackMDSEC\subir
       //let dbf = await DBFFile.open('D:/A-SISTEMA/Desktop/Guindi/controllers/stock.DBF');
       //let dbf = await DBFFile.open('../backMDSEC/subir/BENEFEXP.DBF');
       let dbf = await DBFFile.open('./subir/BENEFEXP.DBF');

       //guardo la cantidad de registros que hay
       let totalreg = dbf.recordCount;
       //console.log(`DBF file contains ${dbf.recordCount} records.`);
       //console.log(`Field names: ${dbf.fields.map(f => f.name).join(', ')}`);
       //leo todos los registros
       let records = await dbf.readRecords(totalreg);
       //actualizo la base de datos
       //for (let record of records) console.log(records);
       records.map(p => ingresar(p));

       async function ingresar(p) {           
        // Ver si existe el beneficiario
        let buscaridbene = p.IDBENE;
        let buscarpmo = p.PMO;
        //console.log("buscar idbene "+buscaridbene)
        const beneficiario = await Beneficiario.findOne({ idbene: buscaridbene, pmo: buscarpmo });
        if(!beneficiario){
            console.log("no lo encuentra ");
            const newbeneficiario = new Beneficiario();
            newbeneficiario.pmo = p.PMO;
            newbeneficiario.idbene = p.IDBENE;
            newbeneficiario.apellido = p.APELLIDO;
            newbeneficiario.nombre = p.NOMBRE;
            newbeneficiario.documento = p.DOCUMENTO;
            newbeneficiario.cuit = p.CUIT;
            newbeneficiario.fechanaci = p.FECHANACI;
            newbeneficiario.sexo = p.SEXO;
            newbeneficiario.genero = p.GENERO;
            newbeneficiario.tel = p.TEL;
            newbeneficiario.exp_labora = p.EXP_LABORA;
            newbeneficiario.explab = p.EXPLAB;
            newbeneficiario.edu_nivel = p.EDU_NIVEL;
            newbeneficiario.edunivel = p.EDUNIVEL;
            newbeneficiario.expl_activ = p.EXPL_ACTIV;
            newbeneficiario.actividad = p.ACTIVIDAD;
            newbeneficiario.expl_subac = p.EXPL_SUBAC;
            newbeneficiario.subacti = p.SUBACTI;
            newbeneficiario.prog_socia = p.PROG_SOCIA;
            newbeneficiario.progsocial = p.PROGSOCIAL;
            newbeneficiario.id_up = p.ID_UP;
            newbeneficiario.referente = p.REFERENTE;
            newbeneficiario.cant_benef = p.CANT_BENEF;
            newbeneficiario.nombreup = p.NOMBREUP;
            newbeneficiario.f_ingreso = p.F_INGRESO;
            newbeneficiario.f_ejecucio = p.F_EJECUCIO;
            newbeneficiario.id_estado = p.ID_ESTADO;
            newbeneficiario.sub_estado = p.SUB_ESTADO;
            newbeneficiario.num_intern = p.NUM_INTERN;
            newbeneficiario.dom_calle = p.DOM_CALLE;
            newbeneficiario.dom_num = p.DOM_NUM;
            newbeneficiario.dom_calle2 = p.DOM_CALLE2;
            newbeneficiario.dom_calle3 = p.DOM_CALLE3;
            newbeneficiario.dom_ref = p.DOM_REF;
            newbeneficiario.idprovinci = p.IDPROVINCI;
            newbeneficiario.nombreprov = p.NOMBREPROV;
            newbeneficiario.codafip = p.CODAFIP;
            newbeneficiario.iddepartam = p.IDDEPARTAM;
            newbeneficiario.nombredepa = p.NOMBREDEPA;
            newbeneficiario.idmunicipi = p.IDMUNICIPI;
            newbeneficiario.nombremuni = p.NOMBREMUNI;
            newbeneficiario.idlocalida = p.IDLOCALIDA;
            newbeneficiario.nombreloca = p.NOMBRELOCA;
            newbeneficiario.codigopost = p.CODIGOPOST;
            newbeneficiario.id_proyect = p.ID_PROYECT;
            newbeneficiario.monto = p.MONTO;
            newbeneficiario.num_proyec = p.NUM_PROYEC;
            newbeneficiario.nombreproy = p.NOMBREPROY;
            newbeneficiario.monto_proy = p.MONTO_PROY;
            await newbeneficiario.save();
        } else{
            console.log("si lo encuentra ")
            beneficiario.pmo = p.PMO;
            beneficiario.apellido = p.APELLIDO;
            beneficiario.nombre = p.NOMBRE;
            beneficiario.documento = p.DOCUMENTO;
            beneficiario.cuit = p.CUIT;
            beneficiario.fechanaci = p.FECHANACI;
            beneficiario.sexo = p.SEXO;
            beneficiario.genero = p.GENERO;
            beneficiario.tel = p.TEL;
            beneficiario.exp_labora = p.EXP_LABORA;
            beneficiario.explab = p.EXPLAB;
            beneficiario.edu_nivel = p.EDU_NIVEL;
            beneficiario.edunivel = p.EDUNIVEL;
            beneficiario.expl_activ = p.EXPL_ACTIV;
            beneficiario.actividad = p.ACTIVIDAD;
            beneficiario.expl_subac = p.EXPL_SUBAC;
            beneficiario.subacti = p.SUBACTI;
            beneficiario.prog_socia = p.PROG_SOCIA;
            beneficiario.progsocial = p.PROGSOCIAL;
            beneficiario.id_up = p.ID_UP;
            beneficiario.referente = p.REFERENTE;
            beneficiario.cant_benef = p.CANT_BENEF;
            beneficiario.nombreup = p.NOMBREUP;
            beneficiario.f_ingreso = p.F_INGRESO;
            beneficiario.f_ejecucio = p.F_EJECUCIO;
            beneficiario.id_estado = p.ID_ESTADO;
            beneficiario.sub_estado = p.SUB_ESTADO;
            beneficiario.num_intern = p.NUM_INTERN;
            beneficiario.dom_calle = p.DOM_CALLE;
            beneficiario.dom_num = p.DOM_NUM;
            beneficiario.dom_calle2 = p.DOM_CALLE2;
            beneficiario.dom_calle3 = p.DOM_CALLE3;
            beneficiario.dom_ref = p.DOM_REF;
            beneficiario.idprovinci = p.IDPROVINCI;
            beneficiario.nombreprov = p.NOMBREPROV;
            beneficiario.codafip = p.CODAFIP;
            beneficiario.iddepartam = p.IDDEPARTAM;
            beneficiario.nombredepa = p.NOMBREDEPA;
            beneficiario.idmunicipi = p.IDMUNICIPI;
            beneficiario.nombremuni = p.NOMBREMUNI;
            beneficiario.idlocalida = p.IDLOCALIDA;
            beneficiario.nombreloca = p.NOMBRELOCA;
            beneficiario.codigopost = p.CODIGOPOST;
            beneficiario.id_proyect = p.ID_PROYECT;
            beneficiario.monto = p.MONTO;
            beneficiario.num_proyec = p.NUM_PROYEC;
            beneficiario.nombreproy = p.NOMBREPROY;
            beneficiario.monto_proy = p.MONTO_PROY;

            await beneficiario.save();
        }
        }
        res.json({ msg: 'Se actualizaron los Datos. Total de registros: '+totalreg });
       
    } catch (error) {
      console.log(error);
      res.status(500).send("hubo un error")    
    }
  }

//listado de 1 beneficiario con pagination
exports.benebuscardocu = async (req, res) => {
  // Validación de campos
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      console.log("entra error 422")
      return res.status(422).json({ errors: errors.array() });
  }
  try {
      const { pagina, porpagina } = req.query;
      const docubuscar = req.header('docubuscar');
      console.log("docubuscar "+docubuscar)
      const options = {
          sort : 'documento',
          populate : 'tirillasid',
          page : parseInt(pagina,10),
          limit : parseInt(porpagina,10),
      }
      const beneficiario = await Beneficiario.paginate({documento: { $eq: docubuscar} } ,options);
      //const producto = await Productos.find({codi: codibuscar});
      if(beneficiario.totalDocs === 0){
          return res.status(404).json({ msg: 'Beneficiario No Encontrado.'});
      }
      console.log(beneficiario);
      res.json({ msg: 'Ok', beneficiario });
  } catch (error) {
      console.log(error);
      return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado 1 Beneficiario.' });
  }
}
//listado de beneficiarios por apellido y nombre con pagination
exports.benebuscarname = async (req, res) => {
    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log("entra error 422")
        return res.status(422).json({ errors: errors.array() });
    }
    try {
        const { pagina, porpagina } = req.query;
        var namebuscar = req.header('namebuscar');
        var name = req.header('name');
        namebuscar = namebuscar.toUpperCase();
        name = name.toUpperCase();
        console.log("namebuscar "+namebuscar+ " " +name);
        const options = {
            sort : 'apellido nombre',
            populate : 'tirillasid',
            page : parseInt(pagina,10),
            limit : parseInt(porpagina,10),
        }
        const beneficiario = await Beneficiario.paginate({ apellido: { $regex: namebuscar}, nombre: { $regex: name} } ,options);
        //const producto = await Productos.find({codi: codibuscar});
        if(beneficiario.totalDocs === 0){
            return res.status(404).json({ msg: 'Beneficiario No Encontrado.'});
        }
        console.log(beneficiario);
        res.json({ msg: 'Ok', beneficiario });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado 1 Beneficiario.' });
    }
  }