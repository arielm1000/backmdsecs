const Tirillas = require("../models/Tirillas");
const Beneficiario = require("../models/Beneficiario");
const { validationResult } = require("express-validator");
const mongoose = require('mongoose');

exports.crearTirillas = async (req, res) => {

    const errores = validationResult(req);
    if (!errores.isEmpty()) {
      //console.log(req.body.detallepedido)
        return res.status(400).json({errores: errores.array()})
    }
    const { idbeneficiario, 
            detalle, 
            fecha,
        } = req.body;
  
    try {
      //me fijo que existe el beneficiario
      let beneficiario = await Beneficiario.findById(idbeneficiario);
      if (!beneficiario) {
        return res.status(400).json({ msg: "Beneficiario invalido." });
      }
      //existe el beneficiario me fijo si esta en ejecucion
      if (beneficiario.id_estado == 0) {
        return res.status(400).json({ msg: "Beneficiario Sin Ingreso." });
      }
      //existe el beneficiario guardo la tirilla
      //let tirillas = new Tirillas(req.body);
      //let tirillas = new Tirillas(idbeneficiario, detalle, fecha.toISOString());
      let tirillas = new Tirillas({detalle, fecha});
  
      //guardar pedido
      await tirillas.save();
      //console.log("tirillas id: " + tirillas._id);
      //guardo el id de la tirilla en el beneficiario insertedId es el id de la tirilla guardada
      await beneficiario.tirillasid.push(tirillas._id);
      //guardo en la base mongo
      await beneficiario.save()
  
      return res.json({ msg: "La Tirilla fue Creado Correctamente.", tirillas });
  
    } catch (error) {
      res.status(400).send("Hubo un error");
    }
};
// Eliminar tirilla
exports.eliminartirilla = async (req, res) => {
  try {
      //console.log(req.params);
      // Validar ID
      if(!mongoose.Types.ObjectId.isValid(req.params.idtirilla)){
          return res.status(404).json({ msg: 'La Tirilla no existe.'});
      }

      // Verificar que la tirilla exista
      let tirilla = await Tirillas.findById(req.params.idtirilla);
      
      if(!tirilla){
          return res.status(404).json({ msg: 'La Tirilla no existe.'});
      }
      // Validar ID
      if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({msg: 'El Beneficiario no existe'});
      }
      //Verifico que exista el Beneficiario 
      let benef = await Beneficiario.findById(req.params.id);
      if(!benef){
        return res.status(404).json({msg: 'El Beneficiario no existe'})
      }
      // Eliminar tirilla
      await tirilla.remove();
      // Eliminar id de la tirilla en propiedad del beneficiario
      await benef.update({$pull : {"tirillasid" : req.params.idtirilla}});

      res.json({msg: 'Tirilla eliminada correctamente.'});

  } catch (error) {
      console.log(error);
      res.status(400).json({ msg: 'Hubo un error.'});
  }
};
// Actualizar Tirillas
exports.modificarTirillas = async (req, res) => {
    
  // Validación de campos
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
  }

  try {
      // Validar ID de la tirilla que pertenezca a ObjectId de Mongoose
      if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({ msg: 'La Tirilla no existe Mongoose.'});
      }
      //Busco la tirilla en mongoose
      let tirilla = await Tirillas.findById(req.params.id);

      if(!tirilla){
        res.status(404).json({ msg: 'La tirilla no existe.'});
      }

      // Modificar tirilla... nota el tercer parametro es para q me devuelva la actualizacion de la tirilla actualizado
      // Si no lo pongo me devuelve la tirilla antes de la modificacion 
      // marca = await Marcas.findByIdAndUpdate(req.params.id, req.body, { new: true });

      tirilla.fecha = req.body.fecha;
      tirilla.detalle = req.body.detalle;
      await tirilla.save();
      res.json({ msg: 'La tirilla se actualizo correctamente.', tirilla });
  } catch (error) {
      console.log(error);
      res.status(400).json({ msg: 'Hubo un error.'});
  }
}