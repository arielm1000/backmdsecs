const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

// Definir Schema
const BeneficiarioSchema = new Schema({
    pmo: {
        type: String,
        required: true,
        trim: true
    },
    idbene: {
        type: Number,
        required: true,
        trim: true
    },
    apellido: {
        type: String,
        required: true,
        trim: true
    },
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    documento: {
        type: String,
        required: false,
        trim: true
    },
    cuit: {
        type: String,
        required: false,
        trim: true
    },
    fechanaci: {
        type: Date,
        required: false
    },
    sexo: {
        type: Number,
        required: true
    },
    genero: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: false
    },
    exp_labora: {
        type: Number,
        required: false
    },
    explab: {
        type: String,
        required: false
    },
    edu_nivel: {
        type: Number,
        required: false
    },
    edunivel: {
        type: String,
        required: false
    },
    expl_activ: {
        type: Number,
        required: false
    },
    actividad: {
        type: String,
        required: false
    },
    expl_subac: {
        type: Number,
        required: false
    },
    subacti: {
        type: String,
        required: false
    },
    prog_socia: {
        type: Number,
        required: false
    },
    progsocial: {
        type: String,
        required: false
    },
    id_up: {
        type: Number,
        required: false
    },
    referente: {
        type: Boolean,
        default: false,
    },
    cant_benef: {
        type: Number,
        required: false
    },
    nombreup: {
        type: String,
        required: false
    },
    f_ingreso: {
        type: Date,
        required: false
    },
    f_ejecucio: {
        type: Date,
        required: false
    },
    id_estado: {
        type: Number,
        required: false
    },
    sub_estado: {
        type: String,
        required: false
    },
    num_intern: {
        type: Number,
        required: false
    },
    dom_calle: {
        type: String,
        required: false
    },
    dom_num: {
        type: String,
        required: false
    },
    dom_calle2: {
        type: String,
        required: false
    },
    dom_calle3: {
        type: String,
        required: false
    },
    dom_ref: {
        type: String,
        required: false
    },
    idprovinci: {
        type: Number,
        required: false
    },
    nombreprov: {
        type: String,
        required: false
    },
    codafip: {
        type: Number,
        required: false
    },
    iddepartam: {
        type: Number,
        required: false
    },
    nombredepa: {
        type: String,
        required: false
    },
    idmunicipi: {
        type: Number,
        required: false
    },
    nombremuni: {
        type: String,
        required: false
    },
    idlocalida: {
        type: Number,
        required: false
    },
    nombreloca: {
        type: String,
        required: false
    },
    codigopost: {
        type: Number,
        required: false
    },
    id_proyect: {
        type: Number,
        required: false
    },
    monto: {
        type: Number,
        required: false
    },
    num_proyec: {
        type: Number,
        required: false
    },
    nombreproy: {
        type: String,
        required: false
    },
    monto_proy: {
        type: Number,
        required: false
    },
    tirillasid: [{
        type: Schema.Types.ObjectId,
        ref: 'Tirillas',
        required: false,
        default: null,
    }],
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});
//esto es para update_at
BeneficiarioSchema.pre('save', async function (){
    console.log(this);
    this.updated_at = new Date();
});
//pagination
BeneficiarioSchema.plugin(mongoosePaginate);
// Definimos el modelo con el Schema
module.exports = mongoose.model('Beneficiario', BeneficiarioSchema);