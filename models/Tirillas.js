const mongoose = require('mongoose');
 
// Definimos el Schema
const TirillasSchema = mongoose.Schema({
    detalle: {
        type: String,
        required: true,
        trim: true
    },
    fecha: {
        type: Date,
        default: Date.now(),
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});
//esto es para update_at
TirillasSchema.pre('save', async function (){
    console.log(this);
    this.updated_at = new Date();
});
 
// Definimos el modelo Tirillas con el schema correspondiente
module.exports = mongoose.model('Tirillas', TirillasSchema);