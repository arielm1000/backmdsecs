const mongoose = require('mongoose')

const RoleSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        unique: true,
        trim: true
    }
})

module.exports = mongoose.model('Role', RoleSchema);