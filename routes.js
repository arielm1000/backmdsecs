const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const auth = require('./middlewares/auth')
const authrole = require('./middlewares/authrole');
var request = require('request');

const usuarioController = require('./controllers/usuarioController');
const authController = require('./controllers/authController');
const beneficiarioController = require('./controllers/beneficiarioController');
const tirillasController = require('./controllers/tirillasController');

// Crear un usuario
// api/usuarios
router.post('/usuarios/',
    [ auth, authrole.authAdmin ],
    [
        check('nombre','El nombre es obligatorio.').not().isEmpty(),
        check('email','El email es obligatorio.').not().isEmpty(),
        check('email','Ingrese un email válido.').isEmail(),
        check('password','El email es obligatorio.').not().isEmpty(),
        check('password','El password debe ser mínimo de 6 caracteres.').isLength({ min: 6})
    ]
    ,usuarioController.crearUsuario
);
// Autenticación de usuarios
// api/auth/
router.post('/auth/',
    [
        check('email','El email es obligatorio.').not().isEmpty(),
        check('password','El email es obligatorio.').not().isEmpty(),
    ]
    ,authController.login
);

//es para guardar el archivo upload en una carpeta subir en el servidor
const path = require('path');
const multer = require('multer');

let storage = multer.diskStorage({ 
    destination: (req, file, cb) => {
        cb(null, './subir')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname );
    }
});

const upload = multer({ storage });

//act filedbf
router.post('/actfiledbf/',
    [ auth, authrole.authAdmin ],
    upload.single('file'), async (req, res) => {
        //console.log(req);
        console.log(req.file);
        console.log('Storage location is ' + req.hostname + ' / ' + req.file.path);
        const {file} = req;
        return res.send({ msg: 'El archivo se subio correctamente.', file });
    }
);
//act beneficiarios
router.get('/actbeneficiarios/',
    [ auth, authrole.authAdmin ],
    beneficiarioController.actbeneficiarios
);
//listado de beneficiarios por documento con pagination
router.get('/benebuscardocu',
    [ auth, authrole.authUser],
    [
        check('docubuscar','El Documento a buscar es Obligatorio.').notEmpty() 
    ],
    beneficiarioController.benebuscardocu
);
//listado de beneficiarios por apellido con pagination
router.get('/benebuscarname',
    [ auth, authrole.authUser],
    [
        check('namebuscar','El Apellido a buscar es Obligatorio.').notEmpty() 
    ],
    beneficiarioController.benebuscarname
);
// Modificar un tirillas
// api/tirillasupdate
router.put('/tirillasupdate/:id',
    [ auth, authrole.authModerator],
    [
        check('detalle','El detalle es obligatorio.').not().isEmpty(),
        check('fecha','La fecha es obligatorio.').not().isEmpty(),
        check('fecha','La fecha tiene que ser valida.').isDate()
    ]
    ,tirillasController.modificarTirillas
);
// Crear un tirillas
// api/tirillas
router.post('/tirillas/',
    [ auth, authrole.authModerator],
    [
        check('detalle','El detalle es obligatorio.').not().isEmpty(),
        check('fecha','La fecha es obligatorio.').not().isEmpty(),
        check('fecha','La fecha tiene que ser valida.').isDate()
    ]
    ,tirillasController.crearTirillas
);
// Eliminar Tirillas
// api/tirillasdele/:id:idtirilla /users/:userId/books/:bookId
router.delete('/tirillasdele/:id/benef/:idtirilla',
    [ auth, authrole.authModerator],
    tirillasController.eliminartirilla
);

//jsreport
router.get('/report/', function(req, res, next){
    var data = {
        template:{'shortid':'9fTE9SadFz'},
        options:{
            preview:true
        }
    }
    var options = {
        uri: 'http://localhost:5488/api/report',
        method: 'POST',
        json:data
    }
    console.log(request(options).pipe(res));
    request(options).pipe(res);
/*     request(options, function (error, response, body) {
        if (error) throw new Error(error);
      
        console.log(body);
        return body;
      }); */
    }

);
module.exports = router;