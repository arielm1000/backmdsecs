const express = require('express');
const morgan = require('morgan');
//import { createRoles } from './libs/initialSetup';
const initialSetup = require('./libs/initialSetup');
 
//Crear el servidor
const app = express();
//Creo los roles en mongoose
initialSetup.createRoles();

//fech
const cors = require('cors');
app.use(cors()); // Habilita CORS
// Puerto de la App
//const PORT = 4000;
const dotenv = require('dotenv');
dotenv.config();

// Puerto de la App
const PORT = process.env.PORT || 4000;
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded

// Rutas
app.use('/api', require('./routes'));

// Middlewares
app.use(morgan('dev'));
 
const mongoose = require('./database');

// Definir la página principal
/* app.get('/', (req, res) => {
    res.send('Hola Mundo');
})
app.get('/usuarios/:id', (req, res) => {
    console.log(req.params);
    res.send('Usuario #'+req.params.id);
}); */
 
// Iniciar la App
app.listen(PORT, () => {
    console.log(`El servidor está funcionando en el puerto ${PORT}`);
});
